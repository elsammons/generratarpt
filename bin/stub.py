#!/usr/bin/env python

import os, sys, xmlrpclib, time, random, fcntl

sys.path.append('/usr/local/lib')
sys.path.append('../lib')

#from cachefile import createCacheFile
#from cachebdb import createdb

import cachesqlite, sqlitecache

try:
    ''' cache files are named CHANNEL_YYYY-MM-DD_YYYY-MM-DD.cache '''
    command = sys.argv[0]
    pid = str(os.getpid())
    
    sd = sys.argv[1]
    ed = sys.argv[2]
    
    p = '/var/tmp/'+command
    
    time.sleep(random.uniform(0,1))
    i = 0
    while True:
        if os.path.exists(p):
            i = i + 1
            time.sleep(random.uniform(1,2))
        else:
            break
       
    try: 
        pf = open(p, 'w+')
        fcntl.lockf(pf.fileno(), fcntl.LOCK_EX)
    except(), e:
        print "ERROR: %s" % e
        sys.exit(1)
        
    try:
        pf.write(pid)
    except(), e:
        print "ERROR: %s" % e
        sys.exit(1)
    
    # channels = ['rhel-x86_64-server-5','rhel-x86_64-as-4', 'redhat-rhn-satellite-5.3-server-x86_64-5']
    channels = ['rhel-x86_64-server-5', 'rhel-x86_64-as-4']
    
    for channel in channels:
        sqlitecache.createErrataTable(channel, sd, ed)
#        cachesqlite.createErrataTable(channel, sd, ed)
#        ''' returns date, advisory, type, synopsis '''
        results = sqlitecache.sqlGetErrata(channel, sd, ed)
        print "**** %s" % channel
        for i in results:
           print "%s %s %s %s %s" % (i['date'], i['type'], i['advisory'], i['synopsis'], i['bzs']) 
    
    try:
        pf.seek(0)
        line = pf.readline()
        if pid in line:
            os.remove(p)
        else:
            sys.exit(1)
    except(OSError), e:
        print "ERROR: %s => %s" % (pid, e)
        sys.exit(1)
    
    sys.exit(0)
except KeyboardInterrupt:
    try:
        os.remove(p)
    except(OSError), e:
        print "ERROR: %s => %s" % (pid, e)
        sys.exit(1)
    sys.exit(2)
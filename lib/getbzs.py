#!/usr/bin/env python

import xmlrpclib

##
# Get Bugzillas associated with errata.
# @param server Server connecting to.
# @param session Your session identifier.
# @param ea Errata to collect BZ information on.
# @return Array

def getBzs(server,session,ea):
	bzs = server.errata.bugzillaFixes(session,ea)
	return bzs	

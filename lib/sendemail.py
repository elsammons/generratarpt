#!/usr/bin/env python

import smtplib,mimetypes
import email.MIMEBase, email.MIMEMultipart, email.Encoders, email.MIMEText

##
# Send email w/ attachement.
# @param f File for attachment.
# @param r Array MAIL RECPT
# @param me MAIL FROM

def sendEmail(f,r,me):
 
#    r = r[1:-2].replace('"','').split(',')
    for i in r:
 
        outer = email.MIMEMultipart.MIMEMultipart('related')
        s = 'Weekly Errata report'
 
#        t,t7 = datestuff()
#        t = str (t)
#        t7 = str (t7)
# 
#        s = s + " " + t7 + " Through " + t
 
        outer['Subject'] = s
        outer['From'] = me
        outer['To'] = i
        outer.preamble = 'This is a multi-part message in MIME Format'
 
        msgAlt = email.MIMEMultipart.MIMEMultipart('alternative')
        outer.attach(msgAlt)
 
        msgText = email.MIMEText.MIMEText('''
        This is your weekly Errata Report. 
        Please let us know if you have any questions or concerns. 
 
        Thank you!''')
 
        msgAlt.attach(msgText)
 
        ctype, encoding = mimetypes.guess_type(f)
        maintype, subtype = ctype.split('/', 1)
 
        fh = open(f, 'rb')
        msg = email.MIMEBase.MIMEBase(maintype, subtype)
        msg.set_payload(fh.read())
        fh.close()
 
        msg.add_header('Content-Disposition', 'attachment', filename=f)
 
 
# 
# Taking the list of recipients and compiling and sending
# the email and file to each, one at a time.
# Not the ideal solution, but couldn't get smtplib to do this for me.
#
 
        email.Encoders.encode_base64(msg)
        outer.attach(msg)
        composed = outer.as_string()
 
        # print 'Sending', f, '\nto: ', i, '\n', 'From : ', me, '\n'
 
        s = smtplib.SMTP()
        s.connect('smtp.corp.redhat.com')
        s.sendmail(me,i,composed)
        s.close

#!/usr/bin/env python

import xmlrpclib

def connectServer(u, p):
    try:
        server = xmlrpclib.Server('https://rhn.redhat.com/rpc/api',verbose=0)
        session = server.auth.login(u, p)
    except xmlrpclib.ProtocolError, detail:
        print "\nError: There is a error with your url:\n\t%s" % detail
        sys.exit(-1)
    except xmlrpclib.Fault, detail:
        print "\nError: %s\n" % detail
        sys.exit(-1)

    return(server, session)
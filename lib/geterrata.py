#!/usr/bin/env python

import xmlrpclib

##
# Get errata for specified channel.
# @param server Server
# @param session Session identifier
# @param channel RHN Channel to get errata for.
# @param sdate Start Date in date range.
# @param edate End Date in date range.

def getErrata(server,session,channel,sdate):
#    e = server.channel.software.listErrata(session,channel,sdate,edate)
    e = server.channel.software.listErrata(session,channel,sdate)
    return e

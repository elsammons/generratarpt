#!/usr/bin/env python

import xmlrpclib, sys

##
# Acquires channels from given RHN Hosted or Satellite address.
# @param server Server
# @param session Session identifier
# @return List of channels with details.

def getChannels(server,session):
	print "\n****    WARNING: this could take a minute or two    ****"
	print "**** You may want to redirect to a file or use less ****\n"

	l = server.channel.listSoftwareChannels(session)
	return l

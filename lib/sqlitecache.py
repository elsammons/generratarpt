#!/usr/bin/env python

import sqlite3, sys, xmlrpclib, re, time
from geterrata import getErrata
import rhnconnect, getbzs, syslog, tempfile

def createErrataTable(server, session, channel, sd, ed):
    """ 
    Table names can not have dashes.
    """
    p = re.compile('-')
    tableName = p.sub('_', channel)
    
    p = re.compile('\.')
    tableName = p.sub('_', tableName)
    
    ''' variables and some modified variables '''
    day = 86400    
    edates = []
    dates = []
    dbdates = []
    
    """We use EPOCH for time format as it is easier to work with
                    when dealing with 'in' and ranges"""
    sde = int(time.mktime(time.strptime(sd, '%Y-%m-%d')))
    ede = int(time.mktime(time.strptime(ed, '%Y-%m-%d')))
    
    """
    Connect and set up db connection.
    """
    conn = sqlite3.connect('/var/tmp/cache.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    
    ''' Create our tables, if they don't exist. '''
    try:
        c.execute('create table ' + tableName + ' (date long, advisory text, type text, synopsis text, bzs text, unique(advisory))')
    except(sqlite3.OperationalError), e:
        pass
    
    try:
        c.execute('create table cached (channel text, date long, unique(channel, date))')
    except(sqlite3.OperationalError), e:
        pass

    ''' this is where the fun begins, we start by initializing our date, we will
        work with every day between sde and ede.'''
    date = sde
    
    ''' Lets get a list of dates we've cached in the past. '''
    try:
        c.execute('SELECT date from cached where channel = ?', [tableName])
    except(), e:
        pass
    
    ''' I really wish that sqlite3 had a better way of doing this with fetchall(), if it does
        exist, I couldn't figure it out. '''
    while True:
        r = c.fetchone()
        if r != None:
            dbdates.append(r['date'])
        else:
            break
    '''
        Loop through all the dates in range Start (sde) to End (ede).
        Include the start and end date as well.
    '''
    while True:
        ''' If date is valid, which there is no reason it shouldn't be, continue.'''
        if date in range(sde, ede) or date == sde or date == ede:
            ''' if the date is not currently in the cached table, work it.'''
            if date not in dbdates: 
                try:
                    ''' Insert date into cached table. '''
                    c.execute('INSERT INTO cached VALUES (?, ?)', (tableName, date))
                except(sqlite3.IntegrityError), e:
                    syslog.syslog(syslog.LOG_WARNING, 'WARN: %s' % e)
                    pass
                except(sqlite3.OperationalError), e:
                    syslog.syslog(syslog.LOG_WARNING, 'WARN: %s' % e)
                    pass
                
                ''' Set up the errata date in the format required, YYYY-MM-DD '''
                errata_date = time.strftime("%Y-%m-%d", time.localtime(date))

                try:
                    ''' if Session exists move on, don't need another. '''
                    try:
                        if session:
                            pass
                    except(NameError), e:
                        print "Error: %s" % e
                        syslog.syslog(syslog.LOG_CRIT, 'ERROR: %s' % e)
                        sys.exit(1)
                    
                    errata = getErrata(server,session,channel,str(errata_date))

                    ''' Do we have errata to process '''
                    if len(errata) > 0:
                        for i in errata:
                            ''' Need to get the date into the right format. '''   
                            mdate,mtime = i['errata_issue_date'].split(" ")
                            mdate = int(time.mktime(time.strptime(mdate, '%Y-%m-%d')))
                            advisory = i['errata_advisory']
                            type = i['errata_advisory_type']
                            synopsis = i['errata_synopsis']
                            
                            bzs = ""
                            bugs = getbzs.getBzs(server,session,i['errata_advisory'])
                            for bug in bugs:
                                bzs = str(bug) + " " + str(bzs)
                            
                            try:
                                ''' Add a cached entry into our sqlite db. so we never have to 
                                    go to rhn for this again. '''
                                c.execute('INSERT INTO ' + tableName + ' VALUES (?, ?, ?, ?, ?)', (mdate, advisory, type, synopsis, bzs))
                                ''' Is the date for this given advisory in the cached db?
                                    if not, insert it now. '''
                                if mdate not in dbdates:
                                    try:
                                        c.execute('INSERT INTO cached VALUES (?, ?)', (tableName, mdate))
                                    except(sqlite3.IntegrityError), e:
                                        syslog.syslog(syslog.LOG_WARNING, 'WARN: %s' % e)
                                        pass
                                    except(sqlite3.OperationalError), e:
                                        syslog.syslog(syslog.LOG_WARNING, 'WARN: %s' % e)
                                        pass
                            except(sqlite3.IntegrityError), e:
                                pass
                            except(sqlite3.OperationalError), e:
                                syslog.syslog(syslog.LOG_WARNING, 'WARN: %s' % e)
                                pass
                except(), e:
                    print "Unexpected error:", sys.exc_info()[0]
                    syslog.syslog(syslog.LOG_ERR, 'ERROR: %s' % e)
                    raise
            ''' Go to the next day. '''
            date += day
        else:
            break
    ''' Clean up by closing commiting and closing connection. '''        
    conn.commit()
    conn.close()
    return()
        
def sqlGetErrata(tempfd, channel, sd, ed):
    ''' Table names can not have dashes.'''
    p = re.compile('-')
    tableName = p.sub('_', channel)
    
    p = re.compile('\.')
    tableName = p.sub('_', tableName)
    
    sde = int(time.mktime(time.strptime(sd, '%Y-%m-%d')))
    ede = int(time.mktime(time.strptime(ed, '%Y-%m-%d')))
    
    ''' create connection ''' 
    conn = sqlite3.connect('/var/tmp/cache.db')
    conn.text_factory = str
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    
    ''' Try and execute a select statement, storing results
        and store results in a list, k/v.'''   
    c.execute('SELECT * from ' + tableName + ' where date >= ? and date <= ?', (sde,ede))
    # We are changing this from an array, which consumes too much memory to a tempfile.
    # We now accept tempfd so that we can update the file.
    while True:
        r = c.fetchone()
        if r != None:
            date = time.strftime("%Y-%m-%d", time.localtime(r['date']))
            # This is the future
            result = date  + '''|''' + r['advisory'] + '''|''' + r['type'] + '''|''' + r['synopsis'] + '''|''' + r['bzs'] + '''\n''' 
            tempfd.write(result)
            tempfd.flush()
            # the future ends here
        else:
            break
        
    conn.close()
    return()

#!/usr/bin/env python

#######################################
# Program    : generrata.py
# Author    : Eric Sammons
# Purpose    : Compile a xls of errata based on 
#               criteria found in ini file.
#######################################


import xmlrpclib 
import sys 
import time
import datetime 
import re
import smtplib
import random
import fcntl
import os
import syslog
import tempfile

sys.path.append('/usr/local/lib')
sys.path.append('../lib')

from optparse import OptionParser
from ConfigParser  import *
from geterrata import getErrata
from getbzs import getBzs
from getchannels import getChannels
from sendemail import sendEmail
import sqlitecache

try:
    from xlwt import *
except ImportError, e:
    print >>sys.stderr, "%s, Try installing the module via easy_install or confirming your path.\n" % e


def error(e,pf,lockfile):
    """
    Prints error message and exists.
    param e Error message to print.
    @return sys.exit -1
    """
    syslog.syslog(syslog.LOG_ERR, "%s" % e)
    print >>sys.stderr, "\n%s\n" % e
    try:
        pf.close()
        os.unlink(lockfile)
    except(OSError), e:
        syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
        print >>sys.stderr, "ERROR: %s => %s" % (pid, e)
        sys.exit(1)
    sys.exit(-1)

def datestuff(t,d):
    d1 = datetime.timedelta(days=-d)
    td = t + d1
    return td

def genReport(server,session,cp,sd,ed):
    """
    Generate the report based on ini file.
    Will save the file as a spreadsheet and call send email 
    which will send resulting file to recipients as an attachment.
    @param server Server details.
    @param session Session details.
    @param cp INI Configuration file.
    @param sd refers to the start date.
    @param ed refers to end date.
    @return report, emlto, emlfrom
    """

    wb = Workbook()

    ''' Define xls styles. '''
    
    n = "HYPERLINK"

    head_font = Font()
    head_font.bold = True
    head_style = XFStyle()
    head_style.font = head_font
    align = Alignment()
    align.horz = Alignment.HORZ_LEFT
    align.vert = Alignment.VERT_TOP
    align.wrap = Alignment.WRAP_AT_RIGHT

    urlfont = Font()
    urlfont.bold = True
    urlfont.underline = Font.UNDERLINE_SINGLE
    urlfont.colour_index = 4
    
    urlstyle = XFStyle()
    urlstyle.font = urlfont
    urlstyle.alignment = align

    cellstyle = XFStyle()
    cellstyle.alignment = align
    head_style.alignment = align
    
    try:
        channels = cp.get("DEFAULT", "channels")
        charray = channels.split()
        emlto = cp.get("DEFAULT", "to")
        emlto = emlto.split()
        emlfrom = cp.get("DEFAULT", "from")
        report = cp.get("DEFAULT", "report")
    except NoOptionError, detail:
        print >>sys.stderr, "\nError: You have a error in you ini file:\n\t%s\n" % detail
        sys.exit(-1)
    except KeyError, detail:
        print >>sys.stderr, "\nError: You have a key error in your ini file:\n\t %s\n" % detail
        sys.exit(-1)

    report = report + "_" + sd + "_" + ed + ".xls"
    # print "\nCreating report %s\n" % report

    ''' Get the errata '''
    for channel in charray:
        
        # Create a new temp file for each channel that is worked on.
        print channel
        try:
            tempfd = tempfile.NamedTemporaryFile()
        except(), e:
            print >>sys.stderr, "\nError creating temp file: %s\n" % e
            sys.exit(-1)
            
        # for testing purposes lets get the name.
        print tempfd.name
        # print "Processing channel %s" % channel
        syslog.syslog(syslog.LOG_INFO, "%s being processed" % channel)
        x = 0
        y = 0
        # Need to test if ws name needs to be pruned or if it is a duplicate
        #    and take the necessary action. 
        #    Truncate channel to 24 characters _or_ 
        #    when duplicate use 20 characters then append a randint (rwsname)
        try:
            ws = wb.add_sheet(channel[0:24])
        except(Exception), e:
            rwsname = channel[0:20] + str(random.randint(100,1000))
            ws = wb.add_sheet(rwsname)

        ''' There has to be a better way to set up the headings. '''

        ws.col(y).width = 0x00d0 + 5500
        ws.write(x,y,"Channel: ",head_style)
        ws.write(x,y+1,channel,head_style)
        ws.col(y).width = 0x00d0 + 5500
        x = x + 1
        ws.write(x,y,"Start Date: ",head_style)
        ws.write(x,y+1,sd,head_style)
        x = x + 1
        ws.write(x,y,"End Date: ",head_style)
        ws.write(x,y+1,ed,head_style)

        errata = ['Errata Date','Advisory Type','Advisory Identifier','Advisory Synopsis','Related Bugzillas']

        x = 10
        for y in range(0,len(errata)):
            ws.col(y).width = 0x00d0 + 5500
            ws.write(x,y,errata[y],head_style)
        x = x + 1
            
        try:
            t1 = time.time()
            sqlitecache.createErrataTable(server,session,channel, sd, ed)
            t2 = time.time()
            print "createErrataTable: %.4f" % (t2 - t1)
        except(), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "%s => %s" % (pid, e)
        #
        # This is the future, update the tempfd and then use it instead of an array.
        #
        t1 = time.time()
        sqlitecache.sqlGetErrata(tempfd, channel, sd, ed)
        t2 = time.time()
        print "sqlGetErrata: %.4f" % (t2 - t1)
        #
        # Reset, go back to the begining of the file
        #
        tempfd.seek(0)
        #
        # We are going to loop through the lines of the file, 
        # in order to save memory we will work with one line at a time.
        #
        t1 = time.time()
        for line in tempfd:
           date, advisory, type, synopsis, bzs =  line.split('|')
           p = re.compile(':')
           ea = p.sub('-', advisory)
           #
           # Create a url of the errata that can be used by pyExcelerator
           # 
           eurl = '"https://rhn.redhat.com/errata/' + ea + '.html";"' + advisory + '"'
           errata = [date,type,eurl,synopsis,bzs]
           # 
           # We have to make sure we get the errata url field (advisory identifier) formatted
           #
           for y in range(0,len(errata)):
               if y == 2:
                   ws.write(x,y,Formula(n + '(' + errata[y] + ')'),urlstyle)
               else:
                   ws.write(x,y,errata[y],cellstyle)
           x = x + 1
        tempfd.close()
    ''' That was _a lot_ of work, but we are done now, lets save the report. '''            
    wb.save(report)
    t2 = time.time()
    print "Report created: %.4f" % (t2 - t1)
    return report,emlto,emlfrom

##
# It all starts here by first parsing the command line.
# Nothing is passed to main(); however, the command line can 
# be quite long with all the options.  Thankfully we only have to do
# this once per ini file.

def main():
    
    #try:
    ''' Helpful variables '''
    command = sys.argv[0]
    command = os.path.basename(command)
    pid = os.getpid()
    lockfile = '/var/tmp/'+command
    
    time.sleep(random.uniform(0,1))
    
    ''' Check for lockfile if it doesn't exist create it.
        if it does exist, sleep and check again. '''
    if not os.path.exists(lockfile):
        try:
            pf = open(lockfile, 'w+')
            fcntl.lockf(pf.fileno(), fcntl.LOCK_EX)
        except(), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "ERROR: %s" % e
            sys.exit(1)
        
    ''' Write process pid to pid file (lockfile) 
        And flush() to disk '''
    try:
        pf.write(str(pid))
        pf.flush()
    except(), e:
        syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
        print >>sys.stderr, "ERROR: %s" % e
        sys.exit(1)
    '''
    Parse options
    '''
    p = OptionParser()
    p.add_option("-l", "--list", dest="list", 
                 help="List Available Red Hat Channels.", action="store_true")
    
    ''' required --config option to indicate the ini file '''
    p.add_option("", "--config", metavar="Configuration file", dest="config",
        help="INI Configuration file")
    p.add_option("-s","--start", metavar="Start Date", dest="sd", 
        help="Start Date as YYYY-MM-DD")
    p.add_option("-e","--end", metavar="End Date", dest="ed", 
        help="End Date as YYYY-MM-DD")
    p.add_option("-d","--days", metavar="Days Back", dest="d", 
        help="Number of days back from today.")
    p.add_option("","--noemail", action="store_true", dest="noemail", 
        default=False, help="Create speadsheet, but do NOT send email.")

    '''
    Break options into opts and args.
    '''
    try:
        opts, args = p.parse_args()
    except: 
        try:
            pf.close()
            os.unlink(lockfile)
        except(OSError), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "ERROR: %s => %s" % (pid, e)
            sys.exit(-1)
        sys.exit(1)

    

    if not opts.config:
        error("Usage: \n\t" + sys.argv[0] + " --config [ini config file]", pf, lockfile)

    ''' Parse Configuration File '''
    cp = ConfigParser()
    
    ''' We have to do this check, because ConfigParser.read() is stupid
        and will return a empty list even if the file doesn't exist. '''
    if not os.path.isfile(opts.config):
        error("Error: " + opts.config + " Does not exist or is not a file!", pf, lockfile)
        
    ''' Lets try and read the configuration (ini) file. '''
    try:
        cp.read(opts.config)
    except(), detail:
        try: 
            pf.close()
            os.unlink(lockfile)
        except(OSError), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "Error: %s => %s" % (pid, e)
        print detail
        

    try:
        url = cp.defaults()['url']
        user = cp.defaults()['user']
        password = cp.defaults()['password']
    except NoOptionError, detail:
        print >>sys.stderr, "\nError: You have a error in your ini file:\n\t%s\n" % detail
        try:
            pf.close()
            os.unlink(lockfile)
        except(OSError), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "Error: %s => %s" % (pid, e)
        sys.exit(-1)
    except KeyError, detail:
        print >>sys.stderr, "\nError: You have a key error in your ini file:\n\t%s\n" % detail
        try: 
            pf.close()
            os.unlink(lockfile)
        except(OSError), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "Error: %s => %s" % (pid, e)
        sys.exit(-1)

    '''
    Variables that are required for connectivity.
    We'll move these into the ini file later and enforce
    a chmod to secure the the ini file.
    '''
    try:
        server = xmlrpclib.Server(url,verbose=0)
        session = server.auth.login(user,password)
    except xmlrpclib.ProtocolError, detail:
        print >>sys.stderr, "\nError: There is a error with your url:\n\t%s" % detail
        sys.exit(-1)
    except xmlrpclib.Fault, detail:
        print >>sys.stderr, "\nError: %s\n" % detail
        sys.exit(-1)
    except:
        sys.exist(-1)


    '''
    List Channels available from RHN
    '''
    if opts.list:
        l = getChannels(server,session)
        print "Channel Name"
        print "===================================="
        for a in l: 
            print "%s" % (a['channel_label'])
        
        try:
            pf.close()
            os.unlink(lockfile)
        except(), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
        sys.exit(0)

    if not opts.d:
        d = 7
    else:
        d = int(opts.d)

    if not opts.ed:
        ed = datetime.date.today()
        td = datestuff(ed,d)
        ed = str(ed)
    else:
        ed = opts.ed
        ''' We have to convert the string ed to a dateformat that can be used
            in the date calculations in datestuff() 
            dfed refers to date formatted ed '''
        edformat = "%Y-%m-%d"
        dfed = time.strptime(ed,edformat)
        dfed = datetime.date(*dfed[:3])
        td = datestuff(dfed,d)

    ''' Perform date calculations. '''
    
    td = str(td)

    if opts.d and opts.sd:
        error("Error:\n\tYou can not specify days back and start\n\tdate in the same command line!\n\tTry " + sys.argv[0] + " --config=[config file] -e [YYYY-MM-DD] -d [N].\n", pf, lockfile)

    if not opts.sd:
        sd = td
    else:
        sd = opts.sd
    # 
    # Build the report, get the email to and from
    # and deliver.
    #
    report,emlto,emlfrom = genReport(server,session,cp,sd,ed)
    if opts.noemail == False:
        syslog.syslog(syslog.LOG_INFO, "Sending email to %s from %s" % (emlto,emlfrom))
        sendEmail(report,emlto,emlfrom)
    
    ''' Read the PID file, confirm pid matches line. '''
    pf.seek(0)
    line = pf.readline()
    if str(pid) in line:
        try:
            pf.close()
            os.unlink(lockfile)
            sys.exit(0)
        except(), e:
            syslog.syslog(syslog.LOG_ERR, "%s => %s" % (pid, e))
            print >>sys.stderr, "ERROR: %s => %s" % (pid, e)
    else:
        print "ERROR: %s Is not PID in %s" % (pid, lockfile)
        sys.exit(1)
    '''
    except(OSError), e:
        print "ERROR: %s => %s" % (pid, e)
        sys.exit(1)
    '''

    pf.close()
    os.unlink(lockfile)
    sys.exit(0)

'''
Execute main()
'''        
if __name__ == "__main__":
    main()
